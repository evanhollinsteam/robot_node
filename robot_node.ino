
/** RF24Mesh_Example.ino by TMRh20
 *
 * This example sketch shows how to manually configure a node via RF24Mesh, and send data to the
 * master node.
 * The nodes will refresh their network address as soon as a single write fails. This allows the
 * nodes to change position in relation to each other and the master node.
 */


#include "RF24.h"
#include "RF24Network.h"
#include "RF24Mesh.h"
#include <SPI.h>
#include <EEPROM.h>
//#include <printf.h>


/**** Configure the nrf24l01 CE and CS pins ****/
RF24 radio(7, 8);
RF24Network network(radio);
RF24Mesh mesh(radio, network);


/**
 * User Configuration: nodeID - A unique identifier for each radio. Allows addressing
 * to change dynamically with physical changes to the mesh.
 *
 * In this example, configuration takes place below, prior to uploading the sketch to the device
 * A unique value from 1-255 must be configured for each node.
 * This will be stored in EEPROM on AVR devices, so remains persistent between further uploads, loss of power, etc.
 *
 **/
#define nodeID 1


uint32_t displayTimer = 0;
char m[255];

struct payload_t {
  unsigned long ms;
  unsigned long counter;
};

void setup() {

  Serial.begin(115200);
  //printf_begin();
  // Set the nodeID manually
  mesh.setNodeID(nodeID);
  // Connect to the mesh
  Serial.println(F("Connecting to the mesh..."));
  mesh.begin();
}



void loop() {

  mesh.update();

  if (network.available()) {
    RF24NetworkHeader header;
    network.peek(header);
    
    uint32_t dat=0;
    char c = 0;
    switch(header.type) {
      // Display the incoming millis() values from the sensor nodes
      case 'M': 
        network.read(header,&dat,sizeof(dat));
        Serial.println(dat);
        break;
      case 'C': 
        network.read(header,&c,sizeof(c));
        Serial.println(c);
        break;
      case 'S': 
        network.read(header,&m, 255*sizeof(char));
        Serial.println((String) m);
        break;
      default: 
        network.read(header,0,0); 
        Serial.println(header.type);
        break;
    }
  }

  // Send to the master node every second
  if (millis() - displayTimer >= 1000) {
    displayTimer = millis();

    const char message[] = "Hello world!";

    // Send an 'M' type message containing the current millis()
    if (!mesh.write(&message, 'S', sizeof(message))) {

      // If a write fails, check connectivity to the mesh network
      if ( ! mesh.checkConnection() ) {
        //refresh the network address
        Serial.println("Renewing Address");
        mesh.renewAddress();
      } else {
        Serial.println("Send fail, Test OK");
      }
    } else {
      Serial.print("Send OK: "); Serial.println(displayTimer);
    }
  }

}





